// new

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
      const root = document.getElementById("root");
      const ul = document.createElement("ul");
  
    books.forEach(function(item) {
        try {
          if (!item.hasOwnProperty("author")) {
            throw new Error("Помилка: Об'єкт не містить властивість 'author'.");
          }
          if (!item.hasOwnProperty("name")) {
            throw new Error("Помилка: Об'єкт не містить властивість 'name'.");
          }
          if (!item.hasOwnProperty("price")) {
            throw new Error("Помилка: Об'єкт не містить властивість 'price'.");
          }
  
          var li = document.createElement("li");
          li.appendChild(document.createTextNode(item.author + " - назва книги: " + "'" + item.name + "'" + ", ціна " + item.price + " $"));
          ul.appendChild(li);
        } catch (error) {
          console.error(error.message);
        }
      });
  
      root.appendChild(ul);